# Implementation strategy

S'impregner du v4 parce qu'il y a des concepts important. 
Il faut avoir lu les notes de version pour bien comprendre les nouveaux concepts métier.

## Diff v3 / v4

* Reconstruire le RGAA v3 en JSON et faire le diff 
* Pas pertinent de faire un "diff transitif" entre les 4 versions de RGAA (trop de risque d'erreur)

## Découpage en lots

Faire une matrice à double entrée :

* En ligne :
    * les tests supprimés
    * les tests ajoutés
    * les tests modifiés
* En colonne : 
    * ce qui est implémenté dans Asqatasun
    * ce qui ne l'est pas
