# RGAA references

## RGAA

Common patterns

* liste des critères et tests 
* glossaire
* cas particuliers : en V4 les cas particuliers sont remontés au niveau des critères (Ex: [voir critère 13.12](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#contenu) )
* notes techniques : en V4 les notes techniques sont remontés au niveau des critères (Ex: [voir critère 1.2](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#contenu) )

## RGAA v4

Published: 2019-10

* [RGAA 4](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/)
  * [RGAA v4 - Critères et tests](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/)
  * [RGAA v4 - Glossaire](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/)
  * [Notes de révision v3.2017 / v4](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/notes-revision/)
* [Dépôt Github RGAA v4](https://github.com/DISIC/RGAA) --> Release, en JSON uniquement :/ 
  * [RGAA v4 JSON - Critères et tests](https://github.com/DISIC/RGAA/blob/master/criteres.json)
  * [RGAA v4 JSON - Glossaire ](https://github.com/DISIC/RGAA/blob/master/glossaire.json) en json
  * [Tickets](https://github.com/DISIC/RGAA/issues)
* [RGAA v4 - Méthodologie de test](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methodologie-test/#contenu)
  * Très utile car **algo** manuelle pour appliquer chaque test RGAA v4
  * [code source](https://github.com/DISIC/RGAA-tests/blob/master/methodologie.md)



## RGAA v3.2017

Published: 2017

* [RGAA v3.2017](http://references.modernisation.gouv.fr/rgaa/)
* [RGAA v3.2017 (2° URL)](https://references.modernisation.gouv.fr/rgaa-accessibilite/)
* [RGAA v3.2017 Glossaire](http://references.modernisation.gouv.fr/rgaa/glossaire.html)
* [RGAA v3.2017 Cas particuliers](http://references.modernisation.gouv.fr/rgaa/cas-particuliers.html)
* [RGAA v3.2017 Notes techniques](http://references.modernisation.gouv.fr/rgaa/notes-techniques.html)
* [Notes de révision v3.2016 / v3.2017](http://references.modernisation.gouv.fr/rgaa/changelog.html)
* [Dépôt Github RGAA v3.2017](https://github.com/DISIC/rgaa_referentiel)
* [Release Github RGAA v3.2017](https://github.com/DISIC/rgaa_referentiel/releases/tag/RGAA3_2017) Contenu = HTML + ods

## RGAA v3.2016

Published: 2016-06-23

* [RGAA v3.2016](http://references.modernisation.gouv.fr/rgaa/2016/criteres.html)
* [RGAA v3.2016 Glossaire](http://references.modernisation.gouv.fr/rgaa/2016/glossaire.html)
* [RGAA v3.2016 Cas particuliers](http://references.modernisation.gouv.fr/rgaa/2016/cas-particuliers.html)
* [RGAA v3.2016 Notes techniques](http://references.modernisation.gouv.fr/rgaa/2016/notes-techniques.html)
* [Notes de révision v3 / v3.2016](http://references.modernisation.gouv.fr/rgaa/2016/changelog.html)
* [Release Github RGAA v3.2016](https://github.com/DISIC/rgaa_referentiel/releases/tag/RGAA3_2016) (dans dépôt du v3.2017) Contenu = HTML + ods

## RGAA v3

Published:  2015-04-29

* [RGAA v3](https://references.modernisation.gouv.fr/referentiel-technique-0)
* [RGAA v3 Glossaire](https://references.modernisation.gouv.fr/referentiel-technique-0#title-2-glossaire)
* [RGAA v3 Cas particuliers](https://references.modernisation.gouv.fr/referentiel-technique-0#title-3-cas-particuliers)
* [RGAA v3 Notes techniques](https://references.modernisation.gouv.fr/referentiel-technique-0#title-4-notes-techniques)
* [Release Github RGAA v3](https://github.com/DISIC/rgaa_referentiel/releases/tag/RGAA3-0) (dans dépôt du v3.2017) Contenu = pseudo-markdown
