# RGAA.4 Glossary vs v3

## CSV format

- column separator: `ø`
- column surround: `"`

example:
```csv
"Ancre"ø"ID"ø"Lettre"ø"TITRE"ø
"collection-de-pages"ø1ø"A"ø"Collection de pages"ø
```
