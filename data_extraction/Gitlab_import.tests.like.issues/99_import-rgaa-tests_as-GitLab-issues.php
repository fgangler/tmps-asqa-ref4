<?php
/**
 * ----------------------------------
 * Import RGAA tests as Gitlab issues
 * ----------------------------------
 * configure :  $gitlaUrl
 *              $gitlabPrivateToken
 *              $gitlaProjectId
 * ----------------------------------
 * @todo check ID test format
 * @todo add recovery management if API requests fail
 * ----------------------------------
 */
declare(strict_types=1);
error_reporting(E_ALL);

require 'vendor/autoload.php';

use Symfony\Component\Filesystem\Filesystem;

// Config
$dataPath = "../RGAA.4_extraction/data";
$referencialPath = "$dataPath/referential";
$referencialEasyPath = "$dataPath/referential_easy-path";
$testIdsPath = "$referencialPath/tests_IDs.txt";
$gitlaUrl = "https://gitlab.example.com";
$gitlabPrivateToken = "<yourGitlabPrivateToken>";
$gitlaProjectId = "<yourGitLabProject_ID>";

// Optionnal config
# $starId = '3.3.2'; // do not import before test n° 3.3.2

// Intialize tools
$filesystem = new Filesystem();

// Process
if(!$filesystem->exists($testIdsPath) && !is_writable($testIdsPath)) {
    throw new Exception("File [ $testIdsPath ] is not available");
}

$notStartAtBeginning = false;
$process = true;
if (isset($starId)) {
    $notStartAtBeginning = true;
    $process = false;
}
$lines = file("$testIdsPath");
foreach($lines as $id) {
    $id = trim($id);
    if ($notStartAtBeginning && $starId === $id) {
        $process = true;
    }
    $idPart = explode('.', $id);
    $topicId = $idPart[0];
    $criterionId = $idPart[1];
    $testId = $idPart[2];
    $idAnchor = "$topicId-$criterionId-$testId";
    $markdownFile = "$referencialEasyPath/$topicId/$criterionId/$testId/test_$id.md";
    if(!$filesystem->exists($markdownFile) && !is_readable($markdownFile)) {
        throw new Exception("File [ $markdownFile ] is not available");
    }
    $markdown = '';
    $markdownLines = file("$markdownFile");
    foreach($markdownLines as $markdownLine) {
        $markdown .= "> $markdownLine";
    }

    // Prepare Issue data
    $post = [
        'title' => "$id",
        'labels'   => 'TODO',
        'description' => "commentaire :
- ...

## v4 - Règle

source : [**$idAnchor** sur numerique.gouv.fr](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#test-$idAnchor)

$markdown

## v3 - Règle



## v3 - Algorithm

voir : https://github.com/Asqatasun/Asqatasun/blob/master/documentation/en/90_Rules/rgaa3.0/


> ### Selection
> 
",
    ];

    $headers = [
        "PRIVATE-TOKEN: $gitlabPrivateToken",
    ];
    $url = "$gitlaUrl/api/v4/projects/$gitlaProjectId/issues";
    $ch = curl_init("$url");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    echo "\n\n\n-------------------> $id\n";
    if ($process === true) {
        $response = curl_exec($ch);
        curl_close($ch);
        print_r($response);
    }
    else {
        echo " --> no processing";
    }
}


