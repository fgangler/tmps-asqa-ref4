# Import RGAA tests as Gitlab issues

## Configure


```bash
vim 99_import-rgaa-tests_as-GitLab-issues.php
```

```php
$dataPath = "../RGAA.4_extraction/data";
$referencialPath = "$dataPath/referential";
$referencialEasyPath = "$dataPath/referential_easy-path";
$testIdsPath = "$referencialPath/tests_IDs.txt";
$gitlaUrl = "https://gitlab.example.com";
$gitlabPrivateToken = "<yourGitlabPrivateToken>";
$gitlaProjectId = "<yourGitLabProject_ID>";
```

## Run

```bash
composer install
php 99_import-rgaa-tests_as-GitLab-issues.php
```

