<?php
declare(strict_types = 1);
error_reporting(E_ALL);

require 'vendor/autoload.php';
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\String\Slugger\AsciiSlugger;
use function Symfony\Component\String\u; // the u() function creates Unicode strings

// Config
$csvDelimiter = 'ø' ;
$builidPath   = 'build/'.date('Y-m-d_H\hi.s');
$builidPath   = 'build/'.date('Y-m-d_H\hi');
$srcPath      = "$builidPath/source";
$dataPath     = "$builidPath/data";
$srcJsonFile  = 'RGAA.v4_glossary_fr.src.json';
$srcHtmlFile  = 'RGAA.v4_glossary_fr.src.html';
$srcJsonUrl   = 'https://raw.githubusercontent.com/DISIC/RGAA/master/glossaire.json';
$srcHtmlUrl   = 'https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/';

// Intialize tools
$filesystem = new Filesystem();
$httpClient = HttpClient::create();
$slugger = new AsciiSlugger();

// Clean up directories
$filesystem->remove("$builidPath");

// Retrieve JSON source file
$response = $httpClient->request('GET', "$srcJsonUrl");
if($response->getStatusCode() !== 200){
    throw new Exception("URL [ $srcJsonUrl ] is not available");
}
$jsonSource = $response->getContent();
$filesystem->dumpFile("$srcPath/$srcJsonFile", "$jsonSource");

// Retrieve HTML source file
$response = $httpClient->request('GET', "$srcHtmlUrl");
if($response->getStatusCode() !== 200){
    throw new Exception("URL [ $srcJsonUrl ] is not available");
}
$htmlSource = $response->getContent();
$filesystem->dumpFile("$srcPath/$srcHtmlFile", "$htmlSource");

// processing data
$json = json_decode($jsonSource);
$id = 0;
$csv = '';
$html = '';
$markdown = '';
foreach ($json->glossary as $sectionObj){
    $letter = $sectionObj->section;
    foreach ($sectionObj->dl as $entry){
        $id++;
        $title = u($entry->dt)->collapseWhitespace()->toString();
        $slugTitle = $slugger->slug($title)->lower();
        $url = "$srcHtmlUrl#$slugTitle";

        // check slug
        $patern = "<dt id=\"$slugTitle\"> $title </dt> ";
        $checkSlug = substr_count($htmlSource, $patern);
        if ($checkSlug !== 1) {
            throw new Exception("ID-$id ---> Anchor [ $checkSlug ] is missing. Title: [ $slugTitle ]\n");
        }

        // data output
        $html .= "$letter - <a href=\"$url\">$title</a><br>\n";
        $markdown .= "- $letter [$title]($url)\n";
        $csv  .= "\"$url\""      . $csvDelimiter
            . "\"$slugTitle\"". $csvDelimiter
            . "\"$id\""       . $csvDelimiter
            . "\"$letter\""   . $csvDelimiter
            . "\"$title\""    . $csvDelimiter
            ."\n";
//        echo  u("$letter -- $slugTitle ")->padEnd(80, '-') . "\n";
    }
}
$csvHead = 'URL'    . $csvDelimiter
        . 'Slug'    . $csvDelimiter
        . 'Id'      . $csvDelimiter
        . 'Letter'  . $csvDelimiter
        . 'title'   . $csvDelimiter
        ."\n";
$finalCsv = $csvHead . $csv;
$finalHtml = "<html>\n<head><title>RGGA.4 - French Glossary</title></head>\n<body>\n$html\n</body>\n</html>";
$finalMarkdown = "# RGGA.4 - French Glossary\n\n$markdown";
$filesystem->dumpFile("$dataPath/RGAA.4_glossary.csv", $finalCsv);
$filesystem->dumpFile("$dataPath/RGAA.4_glossary_summary.html", $finalHtml);
$filesystem->dumpFile("$dataPath/RGAA.4_glossary_summary.md", $finalMarkdown);

// echo  $finalCsv;
// echo  $finalHtml;
// echo  $finalMarkdown;

// Backup files    
$filesystem->remove("./data");
$filesystem->remove("./source");
$filesystem->mirror($dataPath, "./data");
$filesystem->mirror($srcPath, "./source");
