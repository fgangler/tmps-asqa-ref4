<?php
declare(strict_types = 1);
error_reporting(E_ALL);

require 'vendor/autoload.php';
use Michelf\Markdown;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\String\Slugger\AsciiSlugger;
use function Symfony\Component\String\u; // the u() function creates Unicode strings


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Config
$csvDelimiter = ';' ;
$csvDelimiterForAsqatasun = 'ø' ;
$buildPath   = 'build/'.date('Y-m-d_H\hi');
$srcPath      = "$buildPath/source";
$dataPath     = "$buildPath/data";
$referentialPath = "$dataPath/referential";
$asqatasunPath = "$dataPath/asqatasun";
$srcJsonFile  = 'RGAA.v4_Criteria_FR_src-gouv.json';
$srcHtmlFile  = 'RGAA.v4_Criteria_FR_src-gouv.html';
$srcJsonUrl   = 'https://raw.githubusercontent.com/DISIC/RGAA/master/criteres.json';
$srcHtmlUrl   = 'https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/';
$glossaryUrl = "https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/";

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Intialize tools
$filesystem = new Filesystem();
$httpClient = HttpClient::create();
$slugger = new AsciiSlugger();

// Clean up directories
$filesystem->remove("$buildPath");

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Retrieve JSON source file
$response = $httpClient->request('GET', "$srcJsonUrl");
if($response->getStatusCode() !== 200){
    throw new Exception("URL [ $srcJsonUrl ] is not available");
}
$jsonTxt = $response->getContent();
$filesystem->dumpFile("$srcPath/$srcJsonFile", "$jsonTxt");

// Retrieve HTML source file
$response = $httpClient->request('GET', "$srcHtmlUrl");
if($response->getStatusCode() !== 200){
    throw new Exception("URL [ $srcJsonUrl ] is not available");
}
$htmlSource = $response->getContent();
$filesystem->dumpFile("$srcPath/$srcHtmlFile", "$htmlSource");

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Get AA level criteria by browsing HTML source
$crawler = new Crawler($htmlSource);
$htmlIdsForLevel2 = $crawler->filter('[data-level="AA"]')
                      ->extract(['id']);
$criteriaByLevel = [];
foreach ($htmlIdsForLevel2 as $htmlId) {
    $id = u($htmlId)->replace('crit-', '')->replace('-', '.');
    $criteriaByLevel['AA']["$id"] = "$id";
}

// Get criteria equivalents to europe standard by browsing HTML source
$equivalentsToEuropeStandard = getEquivalentsToEuropeStandard($htmlSource);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Prepare directories
$topicDirectories     = Array();
$topicDirectories[1]  = '01.Images';
$topicDirectories[2]  = '02.Frames';
$topicDirectories[3]  = '03.Colours';
$topicDirectories[4]  = '04.Multimedia';
$topicDirectories[5]  = '05.Tables';
$topicDirectories[6]  = '06.Links';
$topicDirectories[7]  = '07.Scripts';
$topicDirectories[8]  = '08.Mandatory_elements';
$topicDirectories[9]  = '09.Structure_of_information';
$topicDirectories[10] = '10.Presentation_of_information';
$topicDirectories[11] = '11.Forms';
$topicDirectories[12] = '12.Navigation';
$topicDirectories[13] = '13.Consultation';
foreach ($topicDirectories as $topicId => $topicDirectory) {
    $topicDirectoriesPath = "$referentialPath/$topicDirectory";
    $easyPath = "$dataPath/referential_easy-path";
    $filesystem->mkdir("$topicDirectoriesPath");
    $filesystem->mkdir("$asqatasunPath/10_documentation/$topicDirectory");
    $filesystem->mkdir("$easyPath");
    $filesystem->symlink("../referential/$topicDirectory","$easyPath/$topicId");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Processing "Topics" by browsing JSON source
$csv = '';
$csvAsqatasunHtml = '';
$csvAsqatasunTxt = '';
$json = json_decode($jsonTxt);
foreach ($json->topics as $topicObj){
    $topicId = (int) $topicObj->number;
    $topicTitle = $topicObj->topic;
    $topicTitle = u($topicTitle)->replace("'", "’")->collapseWhitespace()->toString();
    $topicCriteria = $topicObj->criteria;
    $topicDirectory = $topicDirectories[$topicId];


    //////////////////////////////////////////////////////////
    // Processing of "CRITERIA" for current topic  ///////////
    //////////////////////////////////////////////////////////
    foreach ($topicCriteria as $criterionContainer){

        // Preparing data of criterion
        $criterionObj = $criterionContainer->criterium;
        $criterionId = (int) $criterionObj->number;
        $criterionFullId = "$topicId.$criterionId";
        $criterionMarkdown = trim($criterionObj->title);
        $criterionMarkdown = postProcessingOfMarkdown("$criterionMarkdown", "$glossaryUrl");
        $criterionHtml = trim(Markdown::defaultTransform($criterionMarkdown));
        $criterionHtmlCompact = u($criterionHtml)->collapseWhitespace()->toString();
        $criterionTxt = u(strip_tags($criterionHtml))->collapseWhitespace()->toString();
        $criterionTxt = html_entity_decode($criterionTxt, ENT_QUOTES | ENT_HTML5, 'UTF-8');

        // Get the uses of glossary for current criterion
        $criterionGlossaryAnchors = extractGlossaryAnchorsFromHtml("$criterionHtml", "$glossaryUrl");

        // Saves content of criterion in different formats
        $criterionDirectory = "$referentialPath/$topicDirectory/$criterionId";
        $criterionFilePrefix = "$criterionDirectory/criteria_ $criterionFullId";
        $filesystem->dumpFile("${criterionFilePrefix}.md", "$criterionMarkdown");
        $filesystem->dumpFile("${criterionFilePrefix}.txt", "$criterionTxt");
        $filesystem->dumpFile("${criterionFilePrefix}--HTML.html", "$criterionHtml");
        $filesystem->dumpFile("${criterionFilePrefix}--HTML.min.html", "$criterionHtmlCompact");

        // Processing of "criterion equivalents to europe standard" (comes from the HTML source)
        $equivalentsMarkdown = '';
        foreach ( $equivalentsToEuropeStandard["$criterionFullId"] as $item) {
            $equivalentsMarkdown .= "* $item\n";
        }
        $equivalentsHtml = trim(Markdown::defaultTransform($equivalentsMarkdown));
        $equivalentsHtmlCompact = u($equivalentsHtml)->collapseWhitespace()->toString();
        $equivalentsTxt = u(strip_tags($equivalentsHtml))->collapseWhitespace()->toString();
        $equivalentsFilePrefix = "${criterionFilePrefix}_equivalentsToEuropeStandard";
        $filesystem->dumpFile("${equivalentsFilePrefix}.md", "$equivalentsMarkdown");
        $filesystem->dumpFile("${equivalentsFilePrefix}.txt", "$equivalentsTxt");
        $filesystem->dumpFile("${equivalentsFilePrefix}--HTML.html", "$equivalentsHtml");
        $filesystem->dumpFile("${equivalentsFilePrefix}--HTML.min.html", "$equivalentsHtmlCompact");

        // Processing of "Level" for current criterion
        if(isset($criteriaByLevel['AA']["$criterionFullId"])){
            $criterionLevel = 'AA';
            $filesystem->appendToFile("$referentialPath/criteria_IDs_level-AA.txt", "$criterionFullId\n");
        }
        else {
            $criterionLevel = 'A';
            $filesystem->appendToFile("$referentialPath/criteria_IDs_level-A.txt", "$criterionFullId\n");
        }

        // Save list of criteria
        $criterionLog = "Critère $criterionFullId [ $criterionLevel ] - $criterionTxt";
        $filesystem->appendToFile("$referentialPath/criteria.txt", "$criterionLog\n");
        $filesystem->appendToFile("$referentialPath/criteria_IDs.txt", "$criterionFullId\n");

        // Processing of "Particular Cases" for current criterion
        $flagParticularCases = '';
        if (!isset($criterionObj->particularCases)) {
            $filesystem->appendToFile("$referentialPath/criteria_IDs_withoutParticularCases.txt", "$criterionFullId\n");
        }
        else {
            $flagParticularCases = 'TRUE';
            $criterionParticularCases = $criterionObj->particularCases;
            $filesystem->appendToFile("$referentialPath/criteria_IDs_withParticularCases.txt", "$criterionFullId\n");
            $particularCasesMarkdown = getParticularCasesInMarkdown($criterionParticularCases);
            $particularCasesMarkdown = postProcessingOfMarkdown("$particularCasesMarkdown", "$glossaryUrl");
            $particularCasesHtml = trim(Markdown::defaultTransform($particularCasesMarkdown));
            $particularCasesHtmlCompact = u($particularCasesHtml)->collapseWhitespace()->toString();
            $particularCasesTxt = u(strip_tags($particularCasesHtml))->collapseWhitespace()->toString();
            $particularCasesTxt = html_entity_decode($particularCasesTxt, ENT_QUOTES | ENT_HTML5, 'UTF-8');
            $particularCasesFilePrefix = "${criterionFilePrefix}_particularCases";
            $filesystem->dumpFile("${particularCasesFilePrefix}.md", "$particularCasesMarkdown");
            $filesystem->dumpFile("${particularCasesFilePrefix}.txt", "$particularCasesTxt");
            $filesystem->dumpFile("${particularCasesFilePrefix}--HTML.html", "$particularCasesHtml");
            $filesystem->dumpFile("${particularCasesFilePrefix}--HTML.min.html", "$particularCasesHtmlCompact");
        }

        // Processing of "References" for current criterion
        if (!isset($criterionObj->references)) {
            $filesystem->appendToFile("$referentialPath/criteria_IDs_withoutReferences.txt", "$criterionFullId\n");
        }
        else {
            $criterionReferences = $criterionObj->references;
//          $filesystem->appendToFile("$referentialPath/criteria_IDs_withReferences.txt", "$criterionFullId\n");
//            echo "------------------> $criterionFullId\n";
//            print_r($criterionReferences);
        }

        // Processing of "TechnicalNote" for current criterion
        $flagTechnicalNote = '';
        if (!isset($criterionObj->technicalNote)) {
            $filesystem->appendToFile("$referentialPath/criteria_IDs_withoutTechnicalNote.txt", "$criterionFullId\n");
        }
        else {
            $flagTechnicalNote = 'TRUE';
            $criterionTechnicalNote = $criterionObj->technicalNote;
            $filesystem->appendToFile("$referentialPath/criteria_IDs_withTechnicalNote.txt", "$criterionFullId\n");
//            echo "------------------> $criterionFullId\n";
//            print_r($criterionReferences);
        }

        //////////////////////////////////////////////////////////
        // Processing of "TESTS" for current criterion ///////////
        //////////////////////////////////////////////////////////
        $criterionTests = $criterionObj->tests;
        foreach($criterionTests as $testId => $test){

            // Preparing data of test
            $testLevel = $criterionLevel;
            $testMarkdown = getTestInMarkdown($test);
            $testMarkdown = postProcessingOfMarkdown("$testMarkdown", "$glossaryUrl");
            $testHtml = trim(Markdown::defaultTransform($testMarkdown));
            $testHtmlCompact = u($testHtml)->collapseWhitespace()->toString();
            $testTxt  = u(strip_tags($testHtml))->collapseWhitespace()->toString();
            $testTxt  = html_entity_decode($testTxt, ENT_QUOTES | ENT_HTML5, 'UTF-8');

            // Saves content of test in different formats
            $testFullId = "$topicId.$criterionId.$testId";
            $testDirectory = "$dataPath/referential/$topicDirectory/$criterionId/$testId";
            $testFilePrefix = "${testDirectory}/test_${testFullId}";
            $filesystem->dumpFile("${testFilePrefix}.md", "$testMarkdown");
            $filesystem->dumpFile("${testFilePrefix}.txt", "$testTxt");
            $filesystem->dumpFile("${testFilePrefix}--HTML.html", "$testHtml");
            $filesystem->dumpFile("${testFilePrefix}--HTML.min.html", "$testHtmlCompact");

            // Get the uses of glossary for current test
            $testGlossaryAnchors = extractGlossaryAnchorsFromHtml("$testHtml", "$glossaryUrl");

            // Compute glossary anchors
            $numberOfGlossaryAnchorsForCriteriony = count($criterionGlossaryAnchors);
            $numberOfGlossaryAnchorsForTest = count($testGlossaryAnchors);
            $criterionGlossaryAnchorsTxt = '';
            if($numberOfGlossaryAnchorsForCriteriony > 0){
                $criterionGlossaryAnchorsTxt = '#' .implode(' #', $criterionGlossaryAnchors);
            }
            else {
                $numberOfGlossaryAnchorsForCriteriony = '';
            }
            $testGlossaryAnchorsTxt = '';
            if($numberOfGlossaryAnchorsForTest > 0){
                $testGlossaryAnchorsTxt = '#' .implode(' #', $testGlossaryAnchors);
            }
            else {
                $numberOfGlossaryAnchorsForTest = '';
            }

            // Save list of tests
            $testLog = "Test $testFullId [ $testLevel ] - $testTxt";
            $filesystem->appendToFile("$referentialPath/tests.txt", "$testLog\n");
            $filesystem->appendToFile("$referentialPath/tests_IDs.txt", "$testFullId\n");

            // CSV file
            $csv .= "'$topicId'"                              . $csvDelimiter;
            $csv .= "'$topicTitle'"                           . $csvDelimiter;
            $csv .= "'$testLevel'"                            . $csvDelimiter;
            $csv .= "'$criterionFullId'"                      . $csvDelimiter;
            $csv .= "'$testFullId'"                           . $csvDelimiter;
            $csv .= "'$flagParticularCases'"                  . $csvDelimiter;
            $csv .= "'$flagTechnicalNote'"                    . $csvDelimiter;
            $csv .= "'$numberOfGlossaryAnchorsForCriteriony'" . $csvDelimiter;
            $csv .= "'$numberOfGlossaryAnchorsForTest'"       . $csvDelimiter;
            $csv .= "'$criterionGlossaryAnchorsTxt'"          . $csvDelimiter;
            $csv .= "'$testGlossaryAnchorsTxt'"               . $csvDelimiter;
            $csv .= "'$criterionTxt'"                         . $csvDelimiter;
            $csv .= "'$testTxt'"                              . $csvDelimiter;
            $csv .= "\n";

            // Asqatasun CSV, HTML content
            $csvAsqatasunHtml .= "'$topicId'"                      . $csvDelimiterForAsqatasun;
            $csvAsqatasunHtml .= "'$topicTitle'"                   . $csvDelimiterForAsqatasun;
            $csvAsqatasunHtml .= "'$topicId-$criterionId'"         . $csvDelimiterForAsqatasun;
            $csvAsqatasunHtml .= "'$criterionHtmlCompact'"         . $csvDelimiterForAsqatasun;
            $csvAsqatasunHtml .= "'$topicId-$criterionId-$testId'" . $csvDelimiterForAsqatasun;
            $csvAsqatasunHtml .= "'$testHtmlCompact'"              . $csvDelimiterForAsqatasun;
            $csvAsqatasunHtml .= "'$testLevel'"                    . $csvDelimiterForAsqatasun;
            $csvAsqatasunHtml .= "\n";

            // Asqatasun CSV, Texte content
            $csvAsqatasunTxt .= "'$topicId'"                      . $csvDelimiterForAsqatasun;
            $csvAsqatasunTxt .= "'$topicTitle'"                   . $csvDelimiterForAsqatasun;
            $csvAsqatasunTxt .= "'$topicId-$criterionId'"         . $csvDelimiterForAsqatasun;
            $csvAsqatasunTxt .= "'$criterionTxt'"                 . $csvDelimiterForAsqatasun;
            $csvAsqatasunTxt .= "'$topicId-$criterionId-$testId'" . $csvDelimiterForAsqatasun;
            $csvAsqatasunTxt .= "'$testTxt'"                      . $csvDelimiterForAsqatasun;
            $csvAsqatasunTxt .= "'$testLevel'"                    . $csvDelimiterForAsqatasun;
            $csvAsqatasunTxt .= "\n";
        }
    }
}

// Saves CSV files
$csvHeader =
    'TopicID'                   . $csvDelimiter
    . 'Topic'                   . $csvDelimiter
    . 'Level'                   . $csvDelimiter
    . 'CriterionID'             . $csvDelimiter
    . 'TestID'                  . $csvDelimiter
    . 'ParticularCases'         . $csvDelimiter
    . 'TechnicalNote'           . $csvDelimiter
    . 'CriterionUseGlossary'    . $csvDelimiter
    . 'TestUseGlossary'         . $csvDelimiter
    . 'CriterionGlossaryAnchors'. $csvDelimiter
    . 'TestGlossaryAnchors'     . $csvDelimiter
    . 'Criterion'               . $csvDelimiter
    . 'test'                    . $csvDelimiter
    ."\n";  // example:

$csvAsqatasunHeader =
    'theme'              . $csvDelimiterForAsqatasun
    . 'theme_fr'         . $csvDelimiterForAsqatasun
    . 'critere'          . $csvDelimiterForAsqatasun
    . 'critere-label_fr' . $csvDelimiterForAsqatasun
    . 'test'             . $csvDelimiterForAsqatasun
    . 'test-label_fr'    . $csvDelimiterForAsqatasun
    . 'level'            . $csvDelimiterForAsqatasun
    ."\n";  // example:
            // 1øImagesø1-1øChaque image a-t-elle une alternative textuelle ?ø1-1-1øChaque image (balise img) a-t-elle un attribut alt ?øAAø
$csvTxt = $csvHeader . $csv;
$csvAsqatasunHtml = $csvAsqatasunHeader . $csvAsqatasunHtml;
$csvAsqatasunTxt  = $csvAsqatasunHeader . $csvAsqatasunTxt;
$filesystem->dumpFile("$asqatasunPath/00_referential-creator/RGAA-4.0--HTML.csv", "$csvAsqatasunHtml");
$filesystem->dumpFile("$asqatasunPath/00_referential-creator/RGAA-4.0--TXT.csv", "$csvAsqatasunTxt");
$filesystem->dumpFile("$referentialPath/tests.csv", "$csvTxt");

// Backup files
$filesystem->remove("./data");
$filesystem->remove("./source");
$filesystem->mirror($dataPath, "./data");
$filesystem->mirror($srcPath, "./source");



////////////////// Lib spécifique //////////////////////////////


/**
 * Processes the particular cases content of a criterion
 * from Json data (partly in Markdown format)
 * and returns the content in Markdown format
 *
 * @param array $criterionParticularCases
 * @return string  particular cases of a criterion in Markdown format
 */
function getParticularCasesInMarkdown(array $criterionParticularCases):string {
    $markdown = '';
    foreach ($criterionParticularCases as $lineNumber => $lineContent) {
        if (is_string($lineContent)) { // paragraph
            $markdown .= "\n";
            $markdown .= u($lineContent)->collapseWhitespace()->toString()."\n\n";
        }
        else if (is_object($lineContent)){ // list
            foreach ($lineContent->ul as $item){
                $markdown .= '* ';
                $markdown .= u($item)->collapseWhitespace()->toString()."\n";
            }
        }
        else {
            throw new Exception("Wrong type for criterionParticularCases");
        }
    }
    return u($markdown)->replace("\n\n\n", "\n\n")->toString();
}

/**
 * Processes content of a test
 * from Json data (partly in Markdown format)
 * and returns the content in Markdown format
 *
 * @param string|array $test
 * @return string
 */
function getTestInMarkdown($test):string {
    if (is_array($test)) {
        $testMarkdown = '';
        foreach ($test as $lineNumber => $lineContent) {
            if ($lineNumber !== 0) {
                $testMarkdown .= '* ';
            }
            $testMarkdown .= trim($lineContent)."\n";
            if ($lineNumber === 0) {
                $testMarkdown .= "\n";
            }
        }
    } else {
        $testMarkdown = $test;
    }
    return $testMarkdown;
}

/**
 * Get criteria equivalents to europe standard
 * by browsing HTML source
 *
 * @param string $htmlSource
 * @return array
 */
function getEquivalentsToEuropeStandard(string $htmlSource):array
{
    $crawler = new Crawler($htmlSource);
    $equivalentsData = $crawler->filter('article[data-level]')->each(function (Crawler $parentCrawler, $i) {
        $id = $parentCrawler->extract(['id'])[0];
        $id = (string) u($id)->replace('crit-', '')->replace('-', '.');
        $subCrawler = $parentCrawler->filter('aside')
            ->filterXPath("//h5[.='Correspondances EN 301 549 V2.1.2 (2018-08)']/following-sibling::node()")
            ->filter('ul > li')
            ->extract(['_name', '_text']);
        $equivalentsToEuropeStandard = [];
        foreach ($subCrawler as $item) {
            $equivalentsToEuropeStandard["$id"][] = $item[1];
        }
        return $equivalentsToEuropeStandard;
    });
    $equivalentsToEuropeStandard = [];
    foreach ($equivalentsData as $subData) {
        foreach ($subData as $criterionaId => $items) {
            $equivalentsToEuropeStandard["$criterionaId"] = $items;
        }
    }
    return $equivalentsToEuropeStandard;
}


/**
 * Post processing of Markdown
 * - replace ['] by [’]
 * - replace [link title](#glossaryAnchor)
 *   by      [link title](<glossaryUrl>#glossaryAnchor)
 *
 * @param string $markdown
 * @return string
 */
function postProcessingOfMarkdown(string $markdown, string $glossaryUrl):string
{
    return u($markdown)
        ->replace("'", "’")
        ->replace("](#", "]($glossaryUrl#")
        ->toString();
}

/**
 * Extract glossary anchors from HTML code
 *
 * @param string $html
 * @param string $glossaryUrl
 * @return array
 * @throws \Symfony\Component\String\Exception\InvalidArgumentException
 */
function extractGlossaryAnchorsFromHtml(string $html, string $glossaryUrl):array
{
    $crawler = new Crawler($html);
    $extractUrls = $crawler->filter('a')->extract(['href']);
    $glossaryAnchors = [];
    foreach ($extractUrls as $url) {
        if(u($url)->startsWith("$glossaryUrl#")) {
            $str = u($url)->replace("$glossaryUrl#", "")->trim()->toString();
            $glossaryAnchors["$str"] = $str;
        }
    }
    return $glossaryAnchors;
}