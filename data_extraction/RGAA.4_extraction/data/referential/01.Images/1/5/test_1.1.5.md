Chaque image vectorielle (balise `<svg>`) [porteuse d’information](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#image-porteuse-d-information), vérifie-t-elle ces conditions ?

* La balise `<svg>` possède un attribut WAI-ARIA `role="img"`.
* La balise `<svg>` a une [alternative textuelle](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#alternative-textuelle-image).
