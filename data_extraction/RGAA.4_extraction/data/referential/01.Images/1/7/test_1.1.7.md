Chaque image embarquée (balise `<embed>` avec l’attribut `type="image/…"`) [porteuse d’information](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#image-porteuse-d-information), vérifie-t-elle une de ces conditions ?

* La balise `<embed>` possède une alternative textuelle
* L’élément `<embed>` est immédiatement suivi d’un [lien ou bouton adjacent](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#lien-ou-bouton-adjacent) permettant d’accéder à un [contenu alternatif](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#contenu-alternatif).
* Un mécanisme permet à l’utilisateur de remplacer l’élément `<embed>` par un [contenu alternatif](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#contenu-alternatif).
