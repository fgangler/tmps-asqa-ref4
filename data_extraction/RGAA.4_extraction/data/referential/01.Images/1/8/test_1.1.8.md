Chaque image bitmap (balise `<canvas>`) [porteuse d’information](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#image-porteuse-d-information), vérifie-t-elle une de ces conditions ?

* La balise `<canvas>` possède une alternative textuelle
* Un [contenu alternatif](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#contenu-alternatif) est présent entre les balises `<canvas>` et `</canvas>`
* L’élément `<canvas>` est immédiatement suivi d’un [lien ou bouton adjacent](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#lien-ou-bouton-adjacent) permettant d’accéder à un [contenu alternatif](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#contenu-alternatif).
* Un mécanisme permet à l’utilisateur de remplacer l’élément `<canvas>` par un [contenu alternatif](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#contenu-alternatif).
