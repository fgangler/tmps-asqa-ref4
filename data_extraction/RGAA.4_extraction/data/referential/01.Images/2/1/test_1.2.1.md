Chaque image (balise `<img>`) [de décoration](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#image-de-decoration), sans [légende](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#legende), vérifie-t-elle une de ces conditions ?

* La balise `<img>` possède un attribut alt vide (`alt=""`) et est dépourvue de tout autre attribut permettant de fournir une [alternative textuelle](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#alternative-textuelle-image).
* La balise `<img>` possède un attribut WAI-ARIA `aria-hidden="true"` ou `role="presentation"`.
