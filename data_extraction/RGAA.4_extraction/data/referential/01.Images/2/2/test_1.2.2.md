Chaque [zone non cliquable](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#zone-non-cliquable) (balise `<area>` sans attribut `href`) [de décoration](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#image-de-decoration), vérifie-t-elle une de ces conditions ?

* La balise `<area>` possède un attribut `alt` vide (`alt=""`) et est dépourvue de tout autre attribut permettant de fournir une [alternative textuelle](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#alternative-textuelle-image).
* La balise `<area>` possède un attribut WAI-ARIA `aria-hidden="true"` ou `role="presentation"`.
