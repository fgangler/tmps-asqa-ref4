Chaque image vectorielle (balise `<svg>`) [de décoration](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#image-de-decoration), sans [légende](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#legende), vérifie-t-elle ces conditions ?

* La balise `<svg>` possède un attribut WAI-ARIA `aria-hidden="true"`.
* La balise `<svg>` et ses enfants sont dépourvus d’[alternative textuelle](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#alternative-textuelle-image).
* Les balises `<title>` et `<desc>` sont absentes ou vides.
* La balise `<svg>` et ses enfants sont dépourvus d’attribut `title`.
