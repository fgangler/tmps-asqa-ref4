Chaque image bitmap (balise `<canvas>`) [de décoration](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#image-de-decoration), sans [légende](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#legende), vérifie-t-elle ces conditions ?

* La balise `<canvas>` possède un attribut WAI-ARIA `aria-hidden="true"`.
* La balise `<canvas>` et ses enfants sont dépourvus d’[alternative textuelle](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#alternative-textuelle-image).
* Il n’y a aucun texte faisant office d’[alternative textuelle](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#alternative-textuelle-image) entre `<canvas>` et `</canvas>`.
