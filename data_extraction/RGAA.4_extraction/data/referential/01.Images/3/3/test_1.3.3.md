Pour chaque [bouton](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#bouton-formulaire) de type `image` (balise `<input>` avec l’attribut `type="image"`), ayant une [alternative textuelle](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#alternative-textuelle-image), cette alternative est-elle pertinente (hors cas particuliers) ?

* S’il est présent, le contenu de l’attribut `alt` est pertinent.
* S’il est présent, le contenu de l’attribut `title` est pertinent.
* S’il est présent, le contenu de l’attribut WAI-ARIA `aria-label` est pertinent.
* S’il est présent, le [passage de texte](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#passage-de-texte-lie-par-aria-labelledby-ou-aria-describedby) associé via l’attribut WAI-ARIA `aria-labelledby` est pertinent.
