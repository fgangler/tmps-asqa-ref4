Chaque bouton associé à une image (balise `input` avec l’attribut `type="image"`) utilisée comme [CAPTCHA](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#captcha) vérifie-t-il une de ces conditions ?

* Il existe une autre forme de [CAPTCHA](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#captcha) non graphique, au moins.
* Il existe une autre solution d’accès à la fonctionnalité sécurisée par le [CAPTCHA](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#captcha).
