Chaque image pourvue d’une [légende](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#legende) (balise `<img>`, `<input>` avec l’attribut `type="image"` ou possédant un attribut WAI-ARIA `role="img"` associée à une [légende](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#legende) adjacente), vérifie-t-elle, si nécessaire, ces conditions ?

* L’image (balise `<img>`, `<input>` avec l’attribut `type="image"` ou possédant un attribut WAI-ARIA `role="img"`) et sa [légende](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#legende) adjacente sont contenues dans une balise `<figure>`.
* La balise `<figure>` possède un attribut WAI-ARIA `role="figure"` ou `role="group"`.
* La balise `<figure>` possède un attribut WAI-ARIA `aria-label` dont le contenu est identique au contenu de la [légende](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#legende).
* La [légende](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#legende) est contenue dans une balise `<figcaption>`.
