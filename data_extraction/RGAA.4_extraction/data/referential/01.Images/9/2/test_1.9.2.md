Chaque [image objet](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#image-objet) pourvue d’une [légende](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#legende) (balise `<object>` avec l’attribut type="image/…" associée à une [légende](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#legende) adjacente), vérifie-t-elle, si nécessaire, ces conditions ?

* L’[image objet](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#image-objet) (balise `<object>`) et sa [légende](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#legende) adjacente sont contenues dans une balise `<figure>`.
* La balise `<figure>` possède un attribut WAI-ARIA `role="figure"` ou `role="group"`.
* La balise `<figure>` possède un attribut WAI-ARIA `aria-label` dont le contenu est identique au contenu de la [légende](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#legende).
* La [légende](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#legende) est contenue dans une balise `<figcaption>`.
