Dans chaque page web, le texte et le texte en image en gras d’un taille restituée supérieure ou égale à 18,5px vérifient-ils une de ces conditions (hors cas particuliers) ?

* Le rapport de [contraste](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#contraste) entre le texte et son arrière-plan est de 3:1, au moins.
* Un mécanisme permet à l’utilisateur d’afficher le texte avec un rapport de [contraste](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#contraste) de 3:1, au moins.
