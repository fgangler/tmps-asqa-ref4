Dans le [mécanisme qui permet d’afficher un rapport de contraste](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#mecanisme-qui-permet-d-afficher-un-rapport-de-contraste-conforme) conforme, le rapport de contraste entre le texte et la couleur d’arrière-plan est-il suffisamment élevé ?

