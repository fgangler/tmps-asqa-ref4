Chaque [média temporel](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#media-temporel-type-son-video-et-synchronise) a-t-il, si nécessaire, les [fonctionnalités de contrôle](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#controle-de-la-consultation-d-un-media-temporel) de sa consultation ?

