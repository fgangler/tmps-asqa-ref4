Pour chaque [média temporel](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#media-temporel-type-son-video-et-synchronise), chaque fonctionnalité vérifie-t-elle une de ces conditions ?

* La fonctionnalité est [activable par le clavier et tout dispositif de pointage](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#accessible-et-activable-par-le-clavier-et-tout-dispositif-de-pointage).
* Une fonctionnalité [activable par le clavier et tout dispositif de pointage](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#accessible-et-activable-par-le-clavier-et-tout-dispositif-de-pointage) permettant de réaliser la même action est présente dans la page.
