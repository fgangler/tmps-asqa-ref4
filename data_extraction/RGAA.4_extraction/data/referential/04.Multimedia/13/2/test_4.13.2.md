Chaque [média temporel](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#media-temporel-type-son-video-et-synchronise) et [non temporel](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#media-non-temporel) qui possède une alternative [compatible avec les technologies d’assistance](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#compatible-avec-les-technologies-d-assistance), vérifie-t-il une de ces conditions ?

* L’alternative est adjacente au média temporel ou non temporel.
* L’alternative est accessible via un [lien ou bouton adjacent](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#lien-ou-bouton-adjacent).
* Un mécanisme permet de remplacer le média temporel ou non temporel par son alternative.
