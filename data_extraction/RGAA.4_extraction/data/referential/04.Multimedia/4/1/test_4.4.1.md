Pour chaque [média temporel](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#media-temporel-type-son-video-et-synchronise) synchronisé pré-enregistré ayant des [sous-titres synchronisés](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#sous-titres-synchronises-objet-multimedia), ces sous-titres sont-ils pertinents ?

