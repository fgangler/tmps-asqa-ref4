Chaque [média non temporel](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#media-non-temporel) vérifie-t-il, si nécessaire, une de ces conditions (hors cas particuliers) ?

* Un [lien ou bouton adjacent](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#lien-ou-bouton-adjacent), clairement identifiable, contient l’adresse ([url](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#url)) d’une page contenant une alternative.
* Un [lien ou bouton adjacent](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#lien-ou-bouton-adjacent), clairement identifiable, permet d’accéder à une alternative dans la page.
