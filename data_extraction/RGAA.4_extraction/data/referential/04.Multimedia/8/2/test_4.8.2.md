Chaque [média non temporel](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#media-non-temporel) associé à une alternative vérifie-t-il une de ces conditions (hors cas particuliers) ?

* La page référencée par le [lien ou bouton adjacent](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#lien-ou-bouton-adjacent) est accessible.
* L’alternative dans la page, référencée par le [lien ou bouton adjacent](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#lien-ou-bouton-adjacent), est accessible.
