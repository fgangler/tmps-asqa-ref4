
Il existe une gestion de cas particulier lorsque :

* Le [média non temporel](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#media-non-temporel) est utilisé à des fins décoratives (c’est-à-dire qu’il n’apporte aucune information);
* Le média non temporel est diffusé dans un [environnement maîtrisé](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#environnement-maitrise);
* Le média non temporel est inséré via JavaScript en vérifiant la présence et la version du plug-in, en remplacement d’un [contenu alternatif](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#contenu-alternatif) déjà présent.

Dans ces situations, le critère est non applicable.

