Pour chaque [tableau de données complexe](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#tableau-de-donnees-complexe) un [résumé](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#resume) est-il disponible ?

