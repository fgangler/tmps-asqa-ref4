Pour chaque [tableau de données](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#tableau-de-donnees), chaque [en-tête de lignes](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#en-tete-de-colonne-ou-de-ligne) s’appliquant à la totalité de la ligne vérifie-t-il une de ces conditions ?

* L’[en-tête de lignes](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#en-tete-de-colonne-ou-de-ligne) est structuré au moyen d’une balise `<th>`.
* L’[en-tête de lignes](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#en-tete-de-colonne-ou-de-ligne) est structuré au moyen d’une balise pourvue d’un attribut WAI-ARIA `role="rowheader"`.
