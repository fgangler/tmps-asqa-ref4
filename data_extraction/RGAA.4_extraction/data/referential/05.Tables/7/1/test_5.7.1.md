Pour chaque contenu de balise `<th>` s’appliquant à la totalité de la ligne ou de la colonne, la balise `<th>` respecte-t-elle une de ces conditions (hors cas particuliers) ?

* La balise `<th>` possède un attribut `id` unique.
* La balise `<th>` possède un attribut `scope`.
* La balise `<th>` possède un attribut WAI-ARIA `role="rowheader"` ou `"columnheader"`.
