Pour chaque contenu de balise `<th>` s’appliquant à la totalité de la ligne ou de la colonne et possédant un attribut `scope`, la balise `<th>` vérifie-t-elle une de ces conditions ?

* La balise `<th>` possède un attribut scope avec la valeur `"row"` pour les [en-tête de lignes](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#en-tete-de-colonne-ou-de-ligne).
* La balise `<th>` possède un attribut scope avec la valeur `"col"` pour les [en-tête de colonnes](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#en-tete-de-colonne-ou-de-ligne).
