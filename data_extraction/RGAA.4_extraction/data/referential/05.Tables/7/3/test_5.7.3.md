Pour chaque contenu de balise `<th>` ne s’appliquant pas à la totalité de la ligne ou de la colonne, la balise `<th>` vérifie-t-elle ces conditions ?

* La balise `<th>` ne possède pas d’attribut `scope`.
* La balise `<th>` ne possède pas d’attribut WAI-ARIA `role="rowheader"` ou `"columnheader"`.
* La balise `<th>` possède un attribut `id` unique.
