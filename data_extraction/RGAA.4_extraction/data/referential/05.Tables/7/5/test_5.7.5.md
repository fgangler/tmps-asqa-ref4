Pour chaque balise pourvue d’un attribut WAI-ARIA `role="rowheader"` ou `"columnheader"` dont le contenu s’applique à la totalité de la ligne ou de la colonne, la balise vérifie-t-elle une de ces conditions ?

* La balise possède un attribut WAI-ARIA `role="rowheader"` pour les [en-têtes de lignes](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#en-tete-de-colonne-ou-de-ligne).
* La balise possède un attribut WAI-ARIA `role="columnheader"` pour les [en-têtes de colonnes](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#en-tete-de-colonne-ou-de-ligne).
