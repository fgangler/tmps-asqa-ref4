Chaque [tableau de mise en forme](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#tableau-de-mise-en-forme) (balise `<table>`) vérifie-t-il ces conditions ?

* Le tableau de mise en forme (balise `<table>`) ne contient pas de balises `<caption>`, `<th>`, `<thead>`, `<tfoot>`, `<colgroup>` ou de balises ayant un attribut WAI-ARIA `role="rowheader"`, `role="columnheader"`.
* Les cellules du tableau de mise en forme (balises `<td>`) ne possèdent pas d’attributs `scope`, `headers`, `axis`.
