Chaque [lien texte](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#lien-texte) vérifie-t-il une de ces conditions (hors cas particuliers) ?

* L’[intitulé de lien](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#intitule-ou-nom-accessible-de-lien) seul permet d’en comprendre la fonction et la destination.
* L’[intitulé de lien](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#intitule-ou-nom-accessible-de-lien) additionné au [contexte du lien](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#contexte-du-lien) permet d’en comprendre la fonction et la destination.
