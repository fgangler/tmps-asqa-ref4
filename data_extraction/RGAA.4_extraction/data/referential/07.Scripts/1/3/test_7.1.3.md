Chaque [script](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#script) qui génère ou contrôle un [composant d’interface](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#composant-d-interface) vérifie-t-il ces conditions (hors cas particuliers) ?

* Le composant possède un nom pertinent.
* Le nom accessible du composant contient au moins l’[intitulé visible](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#intitule-visible).
* Le composant possède un rôle pertinent.
