Chaque élément non textuel mis à jour par un [script](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#script) (dans la page, ou dans un [cadre](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#cadre)) et ayant une [alternative](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#alternative-a-script) vérifie-t-il ces conditions ?

* L’alternative de l’élément non textuel est mise à jour.
* L’alternative mise à jour est pertinente.
