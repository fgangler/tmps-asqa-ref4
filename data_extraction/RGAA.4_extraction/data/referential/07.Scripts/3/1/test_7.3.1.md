Chaque élément possédant un gestionnaire d’événement contrôlé par un script vérifie-t-il une de ces conditions (hors cas particuliers) ?

* L’élément est [accessible par le clavier et tout dispositif de pointage](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#accessible-et-activable-par-le-clavier-et-tout-dispositif-de-pointage).
* Un élément [accessible par le clavier et tout dispositif de pointage](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#accessible-et-activable-par-le-clavier-et-tout-dispositif-de-pointage) permettant de réaliser la même action est présent dans la page.
