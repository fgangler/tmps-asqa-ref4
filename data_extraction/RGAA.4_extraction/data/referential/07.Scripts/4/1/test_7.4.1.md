Chaque [script](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#script) qui initie un [changement de contexte](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#changement-de-contexte) vérifie-t-il une de ces conditions ?

* L’utilisateur est averti par un texte de l’action du script et du type de changement avant son déclenchement.
* Le changement de contexte est initié par un bouton (input de type `submit`, `button` ou `image` ou balise `<button>`) explicite.
* Le changement de contexte est initié par un lien explicite.
