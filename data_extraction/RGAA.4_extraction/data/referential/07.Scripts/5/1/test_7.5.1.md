Chaque [message de statut](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#message-de-statut) qui informe de la réussite, du résultat d’une action ou bien de l’état d’une application utilise-t-il l’attribut WAI-ARIA `role="status"` ?

