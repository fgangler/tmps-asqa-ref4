Pour chaque page web, le [type de document](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#type-de-document) (balise `doctype`) est-il valide ?

