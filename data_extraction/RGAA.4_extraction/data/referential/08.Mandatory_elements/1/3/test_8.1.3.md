Pour chaque page web possédant une déclaration de [type de document](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#type-de-document), celle-ci est-elle située avant la balise `<html>` dans le code source ?

