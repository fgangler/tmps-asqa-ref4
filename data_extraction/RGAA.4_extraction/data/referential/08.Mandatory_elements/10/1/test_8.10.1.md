Dans chaque page web, chaque texte dont le [sens de lecture](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#sens-de-lecture) est différent du sens de lecture par défaut est contenu dans une balise possédant un attribut `dir` ?

