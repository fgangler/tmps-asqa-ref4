Pour chaque déclaration de [type de document](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#type-de-document), le code source généré de la page vérifie-t-il ces conditions (hors cas particuliers) ?

* Les balises, attributs et valeurs d’attributs respectent les [règles d’écriture](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#regles-d-ecriture);
* L’imbrication des balises est conforme;
* L’ouverture et la fermeture des balises sont conformes;
* Les valeurs d’attribut id sont uniques dans la page;
* Les attributs ne sont pas doublés sur un même élément.
