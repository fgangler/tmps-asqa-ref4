
Il y a une gestion de cas particulier sur la conformité du code HTML.

Pour accompagner la prise en charge progressive de HTML par les navigateurs, les APIs d’accessibilité et les technologies d’assistance, certains critères peuvent exiger la présence d’attributs ou de balises déclarés « obsolètes » en HTML. Par ailleurs, dans la mesure où des balises ou des attributs déclarés « obsolètes » sont utilisés, ils restent soumis aux autres critères du RGAA (exemple la balise `<marquee>` serait non conforme par rapport au [critère 13.8](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#crit-13-8)) et leur support d’accessibilité doit être vérifié au regard de l’environnement de test (ou « base de référence ») retenu.

