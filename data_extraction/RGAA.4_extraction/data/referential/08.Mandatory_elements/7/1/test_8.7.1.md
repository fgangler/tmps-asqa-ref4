Dans chaque page web, chaque texte écrit dans une langue différente de la [langue par défaut](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#langue-par-defaut) vérifie-t-il une de ces conditions (hors cas particuliers) ?

* L’indication de langue est donnée sur l’élément contenant le texte (attribut `lang` et/ou `xml:lang`).
* L’indication de langue est donnée sur un des éléments parents (attribut `lang` et/ou `xml:lang`).
