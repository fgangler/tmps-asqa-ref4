Pour chaque page web, le code de langue de chaque [changement de langue](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#changement-de-langue) vérifie-t-il ces conditions ?

* Le code de langue est valide.
* Le code de langue est pertinent.
