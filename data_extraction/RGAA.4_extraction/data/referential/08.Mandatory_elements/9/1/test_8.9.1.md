Dans chaque page web les balises (à l’exception de `<div>`, `<span>` et `<table>`) ne doivent pas être utilisées [uniquement à des fins de présentation](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#uniquement-a-des-fins-de-presentation). Cette règle est-elle respectée ?

