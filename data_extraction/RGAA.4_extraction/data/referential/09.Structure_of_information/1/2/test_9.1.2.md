Dans chaque page web, le contenu de chaque [titre](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#titre) (balise `<hx>` ou balise possédant un attribut WAI-ARIA `role="heading"` associé à un attribut WAI-ARIA `aria-level`) est-il pertinent ?

