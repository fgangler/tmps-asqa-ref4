Dans chaque page web, chaque passage de texte constituant un [titre](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#titre) est-il structuré à l’aide d’une balise `<hx>` ou d’une balise possédant un attribut WAI-ARIA `role="heading"` associé à un attribut WAI-ARIA `aria-level` ?

