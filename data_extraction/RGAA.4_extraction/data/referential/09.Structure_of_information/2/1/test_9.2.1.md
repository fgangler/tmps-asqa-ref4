Dans chaque page web, la structure du document vérifie-t-elle ces conditions (hors cas particuliers) ?

* La [zone d’en-tête de la page](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#zone-d-en-tete) est structurée via une balise `<header>`.
* Les [zones de navigation principales et secondaires](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#menu-et-barre-de-navigation) sont structurées via une balise `<nav>`.
* La balise `<nav>` est réservée à la structuration des [zones de navigation principales et secondaires](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#menu-et-barre-de-navigation).
* La [zone de contenu principal](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#zone-de-contenu-principal) est structurée via une balise `<main>`.
* La structure du document utilise une balise `<main>` visible unique.
* La [zone de pied de page](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#zone-de-pied-de-page) est structurée via une balise `<footer>`.
