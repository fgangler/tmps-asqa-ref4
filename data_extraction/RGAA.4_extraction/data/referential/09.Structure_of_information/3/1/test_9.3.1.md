Dans chaque page web, les informations regroupées visuellement sous forme de [liste](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#listes) non ordonnée vérifient-elles une de ces conditions ?

* La liste utilise les balises HTML `<ul>` et `<li>`.
* La liste utilise les attributs WAI-ARIA `role="list"` et `"listitem"`.
