Dans chaque page web, les balises servant à la [présentation de l’information](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#presentation-de-l-information) ne doivent pas être présentes dans le code source généré des pages. Cette règle est-elle respectée ?

