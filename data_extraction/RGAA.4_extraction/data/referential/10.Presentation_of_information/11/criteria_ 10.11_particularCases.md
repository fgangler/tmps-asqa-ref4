
Font exception à ce critère les contenus dont l’agencement requiert deux dimensions pour être compris ou utilisés comme :

* Les images, les graphiques ou les vidéos.
* Les jeux (jeux de plateforme, par exemple).
* Les présentations (type diaporama, par exemple).
* Les tableaux de données (complexes).
* Les interfaces où il est nécessaire d’avoir un ascenseur horizontal lors de la manipulation de l’interface.

Note : la majorité des navigateurs sur les systèmes d’exploitation mobile (Android, iOS) ne gère pas correctement la redistribution en cas de zoom. Dans ce contexte le critère sera considéré comme non applicable sur ces environnements.

