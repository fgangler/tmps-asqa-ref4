
Font exception à ce critère les contenus pour lesquels l’utilisateur n’a pas de possibilité de personnalisation :

* Les sous-titres directement intégrés à une vidéo.
* Les images texte.
* Le texte au sein d’une balise `<canvas>`.
