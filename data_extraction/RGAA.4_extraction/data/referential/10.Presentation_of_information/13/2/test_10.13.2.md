Chaque contenu additionnel qui apparait au survol d’un [composant d’interface](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#composant-d-interface) peut-il être survolé par le pointeur de la souris sans disparaître (hors cas particuliers) ?

