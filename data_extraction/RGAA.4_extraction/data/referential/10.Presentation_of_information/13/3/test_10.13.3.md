Chaque contenu additionnel qui apparaît à la prise de focus ou au survol d’un [composant d’interface](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#composant-d-interface) vérifie-t-il une de ces conditions (hors cas particuliers) ?

* Le contenu additionnel reste visible jusqu’à ce que l’utilisateur retire le pointeur souris ou le focus du contenu additionnel et du [composant d’interface](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#composant-d-interface) ayant déclenché son apparition.
* Le contenu additionnel reste visible jusqu’à ce l’utilisateur déclenche une action masquant ce contenu sans déplacer le focus ou le pointeur souris du [composant d’interface](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#composant-d-interface) ayant déclenché son apparition.
* Le contenu additionnel reste visible jusqu’à ce qu’il ne soit plus valide.
