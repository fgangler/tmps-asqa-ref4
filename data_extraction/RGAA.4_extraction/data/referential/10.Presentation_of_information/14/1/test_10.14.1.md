Dans chaque page web, les contenus additionnels apparaissant au survol d’un [composant d’interface](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#composant-d-interface) via les styles CSS respectent-ils si nécessaire une de ces conditions ?

* Les contenus additionnels apparaissent également à l’activation du composant via le clavier et tout dispositif de pointage.
* Les contenus additionnels apparaissent également à la prise de focus du composant.
* Les contenus additionnels apparaissent également par le biais de l’activation ou de la prise de focus d’un autre composant.
