Dans chaque page web, l’augmentation de la taille des caractères jusqu’à 200%, au moins, doit être possible pour l’ensemble du texte dans la page. Cette règle est-t-elle respectée selon une de ces conditions (hors cas particuliers) ?

* Lors de l’utilisation de la fonction d’agrandissement du texte du navigateur.
* Lors de l’utilisation des fonctions de zoom graphique du navigateur.
* Lors de l’utilisation d’un [composant d’interface](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#composant-d-interface) propre au site permettant d’agrandir le texte ou de zoomer.
