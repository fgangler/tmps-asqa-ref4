Dans chaque page web, chaque lien texte signalé uniquement par la couleur, et dont la nature n’est pas évidente, vérifie-t-il ces conditions ?

* La couleur du lien à un rapport de [contraste](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#contraste) supérieur ou égal à 3:1 par rapport au texte environnant.
* Le lien dispose d’une indication visuelle au survol autre qu’un changement de couleur.
* Le lien dispose d’une indication visuelle au focus autre qu’un changement de couleur.
