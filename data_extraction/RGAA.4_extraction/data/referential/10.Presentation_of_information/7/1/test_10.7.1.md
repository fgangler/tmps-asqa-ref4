Pour chaque élément recevant le focus, la prise de focus vérifie-t-elle une de ces conditions ?

* Le style du focus natif du navigateur n’est pas supprimé ou dégradé.
* Un style du focus défini par l’auteur est visible.
