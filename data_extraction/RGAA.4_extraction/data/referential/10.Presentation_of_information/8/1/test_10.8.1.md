Dans chaque page web, chaque contenu caché vérifie-t-il une de ces conditions ?

* Le [contenu caché](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#contenu-cache) a vocation à être ignoré par les technologies d’assistance.
* Le [contenu caché](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#contenu-cache) n’a pas vocation à être ignoré par les technologies d’assistances et est rendu restituable par les technologies d’assistance suite à une action de l’utilisateur réalisable au clavier ou par tout dispositif de pointage sur un élément précédent le contenu caché ou suite à un repositionnement du focus dessus.
