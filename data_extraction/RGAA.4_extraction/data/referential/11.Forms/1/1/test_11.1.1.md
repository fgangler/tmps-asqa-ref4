Chaque [champ de formulaire](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#champ-de-saisie-de-formulaire) vérifie-t-il une de ces conditions ?

* Le [champ de formulaire](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#champ-de-saisie-de-formulaire) possède un attribut WAI-ARIA `aria-labelledby` référençant un [passage de texte](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#passage-de-texte-lie-par-aria-labelledby-ou-aria-describedby) identifié.
* Le [champ de formulaire](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#champ-de-saisie-de-formulaire) possède un attribut WAI ARIA `aria-label`.
* Une balise `<label>` ayant un attribut `for` est associée au [champ de formulaire](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#champ-de-saisie-de-formulaire).
* Le [champ de formulaire](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#champ-de-saisie-de-formulaire) possède un attribut `title`.
* Un bouton adjacent au [champ de formulaire](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#champ-de-saisie-de-formulaire) lui fournit une étiquette visible et un attribut WAI-ARIA `aria-label`, `aria-labelledby` ou `title` lui fournit un nom accessible.
