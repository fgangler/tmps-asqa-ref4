Les indications du caractère obligatoire de la saisie des champs vérifient-elles une de ces conditions (hors cas particuliers) ?

* Une indication de champ obligatoire est visible et permet d’identifier nommément le champ concerné préalablement à la validation du formulaire.
* Le champ obligatoire dispose de l’attribut aria-required="true" ou required préalablement à la validation du formulaire.
