Les champs obligatoires ayant l’attribut aria-required="true" ou required vérifient-ils une de ces conditions ?

* Une indication de champ obligatoire est visible et située dans l’étiquette associé au champ préalablement à la validation du formulaire.
* Une indication de champ obligatoire est visible et située dans le [passage de texte](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#passage-de-texte-lie-par-aria-labelledby-ou-aria-describedby) associé au champ préalablement à la validation du formulaire.
