Les champs obligatoires ayant l’attribut aria-invalid="true" vérifient-ils une de ces conditions ?

* Une indication de champ obligatoire est visible et située dans l’étiquette associée au champ.
* Une indication de champ obligatoire est visible et située dans le [passage de texte](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#passage-de-texte-lie-par-aria-labelledby-ou-aria-describedby) associé au champ.
