Les champs ayant l’attribut aria-invalid="true" dont la saisie requiert un type de données et/ou de format obligatoires vérifient-ils une de ces conditions ?

* Une instructions ou une indication du type de données et/ou de format obligatoire est visible et située dans la balise `<label>` associée au champ.
* Une instructions ou une indication du type de données et/ou de format obligatoire est visible et située dans le [passage de texte](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#passage-de-texte-lie-par-aria-labelledby-ou-aria-describedby) associé au champ.
