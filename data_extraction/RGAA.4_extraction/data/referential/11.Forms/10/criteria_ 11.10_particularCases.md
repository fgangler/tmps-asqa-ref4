
Le test 11.10.1 sera considéré comme non applicable lorsque le formulaire comporte un seul [champ de formulaire](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#champ-de-saisie-de-formulaire) ou qu’il indique les champs optionnels de manière :

* Visible ;
* Dans la balise `<label>` ou dans la [légende](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#legende) associée au champ.

Dans le cas où l’ensemble des champs d’un formulaire sont obligatoires, le test 11.10.1 reste applicable.

