Pour chaque formulaire qui modifie ou supprime des données, ou qui transmet des réponses à un test ou un examen, ou dont la validation a des conséquences financières ou juridiques, la saisie des données vérifie-t-elle une de ces conditions ?

* L’utilisateur peut modifier ou annuler les données et les actions effectuées sur ces données après la validation du formulaire.
* L’utilisateur peut vérifier et corriger les données avant la validation d’un formulaire en plusieurs étapes.
* Un mécanisme de confirmation explicite, via une case à cocher (balise `<input>` de type checkbox ou balise ayant un attribut WAI-ARIA role="checkbox") ou une étape supplémentaire, est présent.
