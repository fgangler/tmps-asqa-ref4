Chaque formulaire dont la validation modifie ou supprime des données à caractère financier, juridique ou personnel vérifie-t-il une de ces conditions ?

* Un mécanisme permet de récupérer les données supprimées ou modifiées par l’utilisateur.
* Un mécanisme de demande de confirmation explicite de la suppression ou de la modification, via un [champ de formulaire](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#champ-de-saisie-de-formulaire) ou une étape supplémentaire, est proposé.
