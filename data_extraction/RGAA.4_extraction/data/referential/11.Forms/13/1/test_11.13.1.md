Chaque [champ de formulaire](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#champ-de-saisie-de-formulaire) dont l’objet se rapporte à une information concernant l’utilisateur vérifie-t-il ces conditions ?

* Le [champ de formulaire](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#champ-de-saisie-de-formulaire) possède un attribut autocomplete.
* L’attribut autocomplete est pourvu d’une valeur présente dans la liste des valeurs possibles pour l’attribut autocomplete associés à un [champ de formulaire](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#champ-de-saisie-de-formulaire).
* La valeur indiquée pour l’attribut autocomplete est pertinente au regard du type d’information attendu.
