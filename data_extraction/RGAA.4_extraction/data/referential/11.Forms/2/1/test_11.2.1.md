Chaque balise `<label>` permet-elle de connaître la fonction exacte du [champ de formulaire](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#champ-de-saisie-de-formulaire) auquel elle est associée ?

