Les champs de même nature vérifient-ils l’une de ces conditions, si nécessaire ?

* Les champs de même nature sont regroupés dans une balise <fieldset>.
* Les champs de même nature sont regroupés dans une balise possédant un attribut WAI-ARIA `role="group"`.
* Les champs de même nature de type radio (<input type="radio"> ou balises possédant un attribut WAI-ARIA role="radio") sont regroupés dans une balise possédant un attribut WAI-ARIA role="radiogroup" ou "group".
