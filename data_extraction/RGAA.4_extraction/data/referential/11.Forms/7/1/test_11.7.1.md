Chaque [légende](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#legende) associée à un regroupement de champs de même nature est-elle pertinente ?

