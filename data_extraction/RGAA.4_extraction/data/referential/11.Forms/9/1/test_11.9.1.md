L’intitulé de chaque bouton est-il pertinent ?

* S’il est présent, le contenu de l’attribut WAI-ARIA `aria-label` est pertinent.
* S’il est présent, le [passage de texte](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#passage-de-texte-lie-par-aria-labelledby-ou-aria-describedby) lié au bouton via un attribut WAI-ARIA `aria-labelledby` est pertinent.
* S’il est présent, le contenu de l’attribut value d’une balise `<input>` de type submit, reset ou button est pertinent.
* S’il est présent, le contenu de la balise `<button>` est pertinent.
* S’il est présent, le contenu de l’attribut alt d’une balise `<input>` de type image est pertinent.
* S’il est présent, le contenu de l’attribut title est pertinent.
