Chaque [ensemble de pages](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#ensemble-de-pages) vérifie-t-il une de ces conditions (hors cas particuliers) ?

* Un [menu de navigation](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#menu-et-barre-de-navigation) et un [plan du site](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#page-plan-du-site) sont présents.
* Un menu de navigation et un [moteur de recherche](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#moteur-de-recherche-interne-a-un-site-web) sont présents.
* Un moteur de recherche et un plan du site sont présents.
