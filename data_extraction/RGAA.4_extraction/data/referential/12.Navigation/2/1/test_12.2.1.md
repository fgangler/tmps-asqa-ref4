Dans chaque [ensemble de pages](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#ensemble-de-pages), chaque page disposant d’un [menu ou de barres de navigation](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#menu-et-barre-de-navigation) vérifie-t-elle ces conditions (hors cas particuliers) ?

* Le [menu ou de barres de navigation](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#menu-et-barre-de-navigation) est toujours à la même place dans la présentation.
* Le [menu ou de barres de navigation](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#menu-et-barre-de-navigation) se présente toujours dans le même ordre relatif dans le code source.
