
Il existe une gestion de cas particulier lorsque :

* Les pages d’un [ensemble de pages](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#ensemble-de-pages) sont le résultat ou une partie d’un processus (un processus de paiement ou de prise de commande, par exemple) ;
* La page est la page d’accueil ;
* le site web est constitué d’une seule page.

Dans ces situations, le critère est non applicable.

