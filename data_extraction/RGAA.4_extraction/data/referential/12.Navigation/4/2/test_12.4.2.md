Dans chaque [ensemble de pages](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#ensemble-de-pages), la fonctionnalité vers la [page « plan du site »](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#page-plan-du-site) est-elle située à la même place dans la présentation ?

