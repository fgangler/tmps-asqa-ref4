Dans chaque [ensemble de pages](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#ensemble-de-pages), le [moteur de recherche](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#moteur-de-recherche-interne-a-un-site-web) est-il accessible à partir d’une fonctionnalité identique ?

