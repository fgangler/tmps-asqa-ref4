Dans chaque [ensemble de pages](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#ensemble-de-pages), la fonctionnalité vers le [moteur de recherche](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#moteur-de-recherche-interne-a-un-site-web) est-elle située à la même place dans la présentation ?

