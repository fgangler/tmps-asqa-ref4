Dans chaque [ensemble de pages](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#ensemble-de-pages), le [lien d’évitement ou d’accès rapide](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#liens-d-evitement-ou-d-acces-rapide) à la [zone de contenu principal](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#zone-de-contenu-principal) vérifient-il ces conditions (hors cas particuliers) ?

* Le lien est situé à la même place dans la présentation.
* Le lien se présente toujours dans le même ordre relatif dans le code source.
* Le lien est visible à la prise de focus au moins.
