Dans chaque page web, l’[ordre de tabulation](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#ordre-de-tabulation) dans le contenu est-il [cohérent](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#comprehensible-ordre-de-lecture) ?

