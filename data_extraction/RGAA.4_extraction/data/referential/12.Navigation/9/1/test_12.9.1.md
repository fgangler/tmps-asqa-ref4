Dans chaque page web, chaque [élément recevant le focus](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#prise-de-focus) vérifie-t-il une de ces conditions ?

* Il est possible d’atteindre l’élément suivant ou précédent pouvant recevoir le focus avec la touche de tabulation.
* L’utilisateur est informé d’un mécanisme fonctionnel permettant d’atteindre au clavier l’élément suivant ou précédent pouvant recevoir le focus.
