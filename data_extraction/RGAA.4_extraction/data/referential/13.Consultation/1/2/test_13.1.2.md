Pour chaque page web, chaque procédé de [redirection](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#redirection) effectué via une balise `<meta>` est-il immédiat (hors cas particuliers) ?

