Pour chaque page web, chaque procédé de [redirection](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#redirection) effectué via un [script](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#script) vérifie-t-il une de ces conditions (hors cas particuliers) ?

* L’utilisateur peut arrêter ou relancer la redirection.
* L’utilisateur peut augmenter la limite de temps avant la redirection de dix fois, au moins.
* L’utilisateur est averti de l’imminence de la redirection et dispose de vingt secondes, au moins, pour augmenter la limite de temps avant la prochaine redirection.
* La limite de temps avant la redirection est de vingt heures, au moins.
