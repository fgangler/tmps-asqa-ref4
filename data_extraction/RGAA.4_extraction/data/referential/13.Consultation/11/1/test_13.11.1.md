Dans chaque page web, les actions déclenchées au moyen d’un dispositif de pointage sur un point unique de l’écran vérifient-elles l’une de ces conditions (hors cas particuliers) ?

* L’action est déclenchée au moment où le dispositif de pointage est [relâché ou relevé](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#relache-ou-releve);
* L’action est déclenchée au moment où le dispositif de pointage est [pressé ou posé](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#presse-ou-pose) puis annulée lorsque le dispositif de pointage est [relâché ou relevé](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#relache-ou-releve);
* Un mécanisme est disponible pour abandonner (avant achèvement de l’action) ou annuler (après achèvement) l’exécution de l’action.
