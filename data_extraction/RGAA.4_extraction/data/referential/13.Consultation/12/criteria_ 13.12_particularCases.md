
Il existe une gestion de cas particulier lorsque :

* Le mouvement est essentiel à l’accomplissement de la fonctionnalité (ex. podomètre).
* La détection du mouvement est utilisée pour contrôler une fonctionnalité au travers d’une interface compatible avec l’accessibilité.
