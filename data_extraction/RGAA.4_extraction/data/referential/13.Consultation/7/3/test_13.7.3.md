Dans chaque page web, chaque mise en forme CSS qui provoque un [changements brusques de luminosité ou les effets de flash](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/glossaire/#changement-brusque-de-luminosite-ou-effet-de-flash) vérifie-t-elle une de ces conditions ?

* La fréquence de l’effet est inférieure à 3 par seconde.
* La surface totale cumulée des effets est inférieure ou égale à 21824 pixels.
