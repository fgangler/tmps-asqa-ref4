Dans chaque page web, chaque contenu vérifie-t-il ces conditions (hors cas particuliers) ?

* La consultation est possible quel que soit le mode d’orientation de l’écran.
* Le contenu proposé reste le même quel que soit le mode d’orientation de l’écran utilisé même si sa présentation et le moyen d’y accéder peut différer.
